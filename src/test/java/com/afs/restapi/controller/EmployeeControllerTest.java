package com.afs.restapi.controller;

import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanEmployeeList() {
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employees_when_getAllEmployees_given_employees() throws Exception {
        // Given
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        employeeRepository.insert(john);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees"))    // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value(john.getGender()))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()));
    }

    @Test
    void should_return_specific_gender_employees_when_findEmployeesByGender_given_employees() throws Exception {
        // Given
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        Employee emily = new Employee(null, "Emily Brown", 23, "Female", 4500.0);
        employeeRepository.insert(john);
        employeeRepository.insert(emily);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "Female"))    // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(emily.getId()))
                .andExpect(jsonPath("$[0].name").value(emily.getName()))
                .andExpect(jsonPath("$[0].age").value(emily.getAge()))
                .andExpect(jsonPath("$[0].gender").value(emily.getGender()))
                .andExpect(jsonPath("$[0].salary").value(emily.getSalary()));
    }

    @Test
    void should_return_created_employee_when_insertEmployee_given_employee_json() throws Exception {
        // Given
        Employee john = new Employee(null, "John Smith", 22, "Male", 5000.0);
        String johnJson = new ObjectMapper().writeValueAsString(john);
        // When
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson)
                )    // Then
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(john.getName()))
                .andExpect(jsonPath("$.age").value(john.getAge()))
                .andExpect(jsonPath("$.gender").value(john.getGender()))
                .andExpect(jsonPath("$.salary").value(john.getSalary()))
                .andExpect(jsonPath("$.active").value(true))
        ;
    }

    @Test
    void should_return_specific_employee_when_getEmployeeById_given_employee_id() throws Exception {
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        employeeRepository.insert(john);
        client.perform(MockMvcRequestBuilders.get("/employees/" + john.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(john.getName()));
    }

    @Test
    void should_return_updated_employee_when_updateEmployee_given_employee_id() throws Exception {
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        employeeRepository.insert(john);

        Employee updatedJohn = new Employee(null, null, 33, null, 6000.0);
        String updatedJohnJson = new ObjectMapper().writeValueAsString(updatedJohn);

        client.perform(MockMvcRequestBuilders.put("/employees/" + john.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedJohnJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.age").value(updatedJohn.getAge()))
                .andExpect(jsonPath("$.salary").value(updatedJohn.getSalary()))
                .andExpect(jsonPath("$.active").value(updatedJohn.isActive()))
        ;

        Assertions.assertEquals(updatedJohn.getAge(), john.getAge());
        Assertions.assertEquals(updatedJohn.getSalary(), john.getSalary());

    }

    @Test
    void should_return_NotFoundException_when_updateEmployee_given_inexist_employee_id() throws Exception {

        Employee updatedJohn = new Employee(10L, null, 33, null, 6000.0);
        String updatedJohnJson = new ObjectMapper().writeValueAsString(updatedJohn);

        client.perform(MockMvcRequestBuilders.put("/employees/" + updatedJohn.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedJohnJson))
                .andExpect(status().isBadRequest());

    }

    @Test
    void should_set_to_inactive_when_deleteEmployee_given_employee_id() throws Exception {
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        employeeRepository.insert(john);
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", john.getId()))
                .andExpect(status().isNoContent());

        Assertions.assertFalse(john.isActive());

    }

    @Test
    void should_return_employees_in_page_when_findByPage_given_page_and_size() throws Exception {
        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        Employee jane = new Employee(null, "Jane Johnson", 28, "Female", 6000.0);
        Employee david = new Employee(null, "David Williams", 35,"Male", 5500.0);
        Employee emily = new Employee(null, "Emily Brown", 23, "Female", 4500.0);
        Employee michael = new Employee(null, "Michael Jones", 40, "Male", 7000.0);
        employeeRepository.insert(john);
        employeeRepository.insert(jane);
        employeeRepository.insert(david);
        employeeRepository.insert(emily);
        employeeRepository.insert(michael);

        client.perform(MockMvcRequestBuilders.get("/employees")
                        .param("page","1")
                        .param("size", "3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[1].id").value(jane.getId()))
                .andExpect(jsonPath("$[1].name").value(jane.getName()))
                .andExpect(jsonPath("$[2].id").value(david.getId()))
                .andExpect(jsonPath("$[2].name").value(david.getName()))
        ;

    }
}
