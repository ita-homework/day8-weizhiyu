package com.afs.restapi;

import com.afs.restapi.exception.InvalidAgeException;
import com.afs.restapi.exception.SalaryNotMatchAgeException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {

    private final EmployeeRepository employeeRepositoryMock = mock(EmployeeRepository.class);

    @Test
    void should_not_create_successfully_when_create_employee_given_an_invalid_age() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null, "mike", 15, "Male", 100.0);
        Assertions.assertThrows(InvalidAgeException.class, () -> employeeService.createEmployee(employee));
    }

    @Test
    void should_not_create_successfully_when_create_employee_given_age_over_30_and_salary_below_20000() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null, "john", 35, "Male", 1000.0);
        Assertions.assertThrows(SalaryNotMatchAgeException.class, () -> employeeService.createEmployee(employee));
    }

    @Test
    void should_set_inactive_when_delete_employee_given_employee_id() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null, "john", 35, "Male", 1000.0);
        employee.setActive(true);

        given(employeeRepositoryMock.findById(1L)).willReturn(employee);
        
        employeeService.delete(1L);
        verify(employeeRepositoryMock, times(1)).findById(1L);
        verify(employeeRepositoryMock, times(1)).update(argThat(updatedEmployee -> {
            Assertions.assertEquals(false, updatedEmployee.isActive());
            return true;
        }));
    }

    @Test
    void should_set_active_when_create_employee_given_employee_json() {
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);
        Employee employee = new Employee(null, "john", 35, "Male", 20000.0);
        employee.setId(6L);

        given(employeeRepositoryMock.insert(employee)).willReturn(employee);

        employeeService.createEmployee(employee);
        verify(employeeRepositoryMock, times(1)).insert(employee);
    }

    @Test
    void should_throw_EmployeeNotFoundException_when_update_not_exist_employee_given_updated_employee_and_id() {
        Employee updatedEmployee = new Employee(null, "john", 35, "Male", 1000.0);
        updatedEmployee.setId(1L);
        EmployeeService employeeService = new EmployeeService(employeeRepositoryMock);

        given(employeeRepositoryMock.findById(1L)).willReturn(updatedEmployee);
        given(employeeRepositoryMock.update(updatedEmployee)).willReturn(updatedEmployee);

        employeeService.update(1L, updatedEmployee);

        verify(employeeRepositoryMock, times(1)).update(updatedEmployee);
    }

}
