package com.afs.restapi.controller;


import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository;

    public CompanyController(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @GetMapping
    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    @GetMapping("/{companyId}")
    public Company findById(@PathVariable Long companyId) {
        return companyRepository.findById(companyId);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long companyId) {
        return companyRepository.getEmployeesByCompanyId(companyId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getByPage(@RequestParam Integer page,
                                   @RequestParam Integer size) {
        return companyRepository.findByPage(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company insert(@RequestBody Company company) {
        return companyRepository.addCompany(company);
    }

    @PutMapping("/{companyId}")
    public Company update(@PathVariable Long companyId, @RequestBody Company company) {
        return companyRepository.updateCompany(companyId, company);
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long companyId) {
        companyRepository.delete(companyId);
    }
}
