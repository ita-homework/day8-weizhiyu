package com.afs.restapi.service;

import com.afs.restapi.exception.InvalidAgeException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.SalaryNotMatchAgeException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        Employee targetEmployee = employeeRepository.findById(id);
        if (targetEmployee == null) {
            throw new EmployeeNotFoundException();
        }
        return targetEmployee;
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findByPage(page, size);
    }

    public Employee createEmployee(Employee employee) {
        if (!employee.isValidAge()) {
            throw new InvalidAgeException();
        }
        if (!employee.isSalaryMatchAge()) {
            throw new SalaryNotMatchAgeException();
        }
        employee.setActive(true);
        return employeeRepository.insert(employee);
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = findById(id);
        if (employeeToUpdate == null) {
            throw new EmployeeNotFoundException();
        }
        employeeToUpdate.merge(employee);
        return employeeRepository.update(employeeToUpdate);
    }

    public void delete(Long id) {
        Employee toRemovedEmployee = findById(id);
        if (toRemovedEmployee == null) {
            throw new EmployeeNotFoundException();
        }
        toRemovedEmployee.setActive(false);
        employeeRepository.update(toRemovedEmployee);
    }
}
