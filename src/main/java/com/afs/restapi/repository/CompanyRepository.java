package com.afs.restapi.repository;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private static final List<Company> companies = new ArrayList<>();
    private final AtomicLong atomicID = new AtomicLong(2L);
    @Resource
    private EmployeeRepository employeeRepository;

    public CompanyRepository() {
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "boot"));
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company addCompany(Company company) {
        company.setId(atomicID.incrementAndGet());
        companies.add(company);
        return company;
    }

    public Company findById(Long companyId) {
        return companies.stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId) {
        return employeeRepository.findAll().stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

    public Company updateCompany(Long companyId, Company company) {
        Company updatedCompany = findById(companyId);
        updatedCompany.setCompanyName(company.getCompanyName());
        return updatedCompany;
    }

    public void delete(Long companyId) {
        companies.removeIf(company -> company.getId().equals(companyId));
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public void clearAll() {
        companies.clear();
    }

}

