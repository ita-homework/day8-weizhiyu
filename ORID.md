# O
- Spring Boot Architecture: The spring boot consists of the following four layers:

    1.Presentation Layer – Authentication & Json Translation 
    
    2.Business Layer – Business Logic, Validation & Authorization
  
    3.Persistence Layer – Storage Logic

    4.Database Layer – Actual Database
- Test Pyramid: Its essential point is that you should have many more low-level UnitTests than high level BroadStackTests running through a GUI. Test pyramid consists of three layers:
  
    1.Unit Tests
  
    2.Service Tests

    3.User Interface Tests
# R
I am a little frustrated.
# I
I'm not familiar with annotations.
# D
I will spend some time to learn annotations.